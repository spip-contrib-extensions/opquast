<?php
/**
 * Définit les autorisations du plugin Check-list Opquast
 *
 * @plugin     Check-list Opquast
 * @copyright  2020
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Opquast\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 *
 * @pipeline autoriser
 */
function opquast_autoriser() {
}

// -----------------
// Objet opquast_criteres

/**
 * Autorisation de voir un élément de menu (opquastcriteres)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_opquastcriteres_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir (opquastcritere)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_opquastcritere_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de changer le statut (opquastcritere)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_opquastcritere_instituer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['webmestre']=='oui' || in_array($qui['id_auteur'],opquast_get_admins_ids());
}

/**
 * Autorisation de créer un logo (opquastcritere)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_opquastcritere_iconifier_dist($faire, $type, $id, $qui, $opt) {
	return false;
}
