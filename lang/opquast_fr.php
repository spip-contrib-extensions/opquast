<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteurs_autorises'             => 'Auteurs autorisés',
	'auteurs_autorises_explication' => '(numéros séparés par un espace ou une virgule)',

	// O
	'opquast_titre'                 => 'Check-list Opquast',
	'opquast_tag'                   => 'Mot clé',
	'opquast_theme'                 => 'Thématique',
	'opquast_etape'                 => 'Étape',

	// C
	'cfg_titre_parametrages'        => 'Paramétrages',

	// S
	'statut'                        => 'Statut',

	// T
	'titre_page_configurer_opquast' => 'Configuration',

	// V
	'version_referentiel'           => 'Version du référentiel à utiliser',
);
