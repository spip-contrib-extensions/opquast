<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_titre_label' => 'Titre',
	'champ_texte_label' => 'Texte',
	'champ_objectif_label' => 'Objectif',
	'champ_solution_label' => 'Solution',
	'champ_controle_label' => 'Contrôle',
	'critere_numero' => 'Critère n° @n@',

	// I
	'info_1_opquast_critere' => 'Un critère',
	'info_aucun_opquast_critere' => 'Aucun critère',
	'info_nb_opquast_criteres' => '@nb@ critères',

	// T
	'texte_changer_statut_opquast_critere' => 'Ce critère est :',
	'texte_statut_conforme' => 'Conforme',
	'texte_statut_non_conforme' => 'Non conforme',
	'texte_statut_non_applicable' => 'Non applicable',
	'texte_statut_non_verifie' => 'Non vérifié',
	'titre_opquast_critere' => 'Critère',
	'titre_opquast_criteres' => 'Critères',
	'tous_statuts' => 'Tous les statuts',
	
	// V
	'voir_en_ligne' => 'Voir en ligne',
	
);
