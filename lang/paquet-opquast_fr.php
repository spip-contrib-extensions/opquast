<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'opquast_description' => 'La check-list [Opquast->https://www.opquast.com/] v4, 240 bonnes pratiques pour améliorer vos sites et mieux prendre en compte vos utilisateurs',
	'opquast_nom' => 'Check-list Opquast',
	'opquast_slogan' => 'Qualité Web : la check-list de référence',
);
