<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Check-list Opquast
 *
 * @plugin     Check-list Opquast
 * @copyright  2020
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Opquast\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin Check-list Opquast.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
 **/
function opquast_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(
		array(
			'maj_tables',
			array(
				'spip_opquast_criteres',
				'spip_opquast_criteres_liens',
				'spip_opquast_tags',
				'spip_opquast_themes',
				'spip_opquast_etapes',
			),
		),
		array('populate_opquast_criteres'),
	);

	$maj['1.1.0'] = array(
		array(
			'maj_tables',
			array(
				'spip_opquast_criteres',
				'spip_opquast_criteres_liens',
				'spip_opquast_tags',
				'spip_opquast_themes',
				'spip_opquast_etapes',
			),
		),
	);

	$maj['2.0.0'] = array(
		array('populate_opquast_criteres_v4'),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin Check-list Opquast.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 *
 * @return void
 **/
function opquast_vider_tables($nom_meta_base_version) {
	sql_drop_table('spip_opquast_criteres');
	sql_drop_table('spip_opquast_criteres_liens');
	sql_drop_table('spip_opquast_tags');
	sql_drop_table('spip_opquast_themes');
	sql_drop_table('spip_opquast_etapes');

	effacer_meta($nom_meta_base_version);
}

/**
 * Fonction d'installation des critères du plugin Check-list Opquast.
 *
 * @return void
 **/
function populate_opquast_criteres() {
	if ($data = file_get_contents(__DIR__ . '/data/data-fr.json')) {
		$data = json_decode($data, true);
	}
	if ($content = file_get_contents(__DIR__ . '/data/data-fr-content.json')) {
		$content = json_decode($content, true);
	}
	foreach ($data as $row) {
		$texte              = (isset($content[$row['id']]) ? $content[$row['id']] : '');
		$texte              = str_replace('<ul>', '<ul class="spip">', $texte);
		$critere            = array(
			'titre'  => $row['title'],
			'lien'   => $row['link'],
			'texte'  => $texte,
			'statut' => 'non_verifie',
		);
		$id_opquast_critere = sql_insertq('spip_opquast_criteres', $critere);

		$tags = explode(',', $row['tags']);
		foreach ($tags as $tag) {
			if (!$id_opquast_tag = sql_getfetsel('id_opquast_tag', 'spip_opquast_tags', 'titre = ' . sql_quote($tag))) {
				$id_opquast_tag = sql_insertq('spip_opquast_tags', array('titre' => $tag));
			}
			sql_insertq('spip_opquast_criteres_liens', array('id_opquast_critere' => $id_opquast_critere, 'objet' => 'opquast_tag', 'id_objet' => $id_opquast_tag));
		}
	}
}

function populate_opquast_criteres_v4() {
	include_spip('action/editer_objet');
	include_spip('action/editer_liens');

	sql_update('spip_opquast_criteres', array('numero' => 'id_opquast_critere', 'version' => sql_quote('v3')));

	// utiliser la nouvelle liste par défaut
	ecrire_config('opquast/version','v4');
	
	if ($data = file_get_contents(__DIR__ . '/data/data-fr-v4.json')) {
		$data = json_decode($data, true);
	}
	foreach ($data as $key => $row) {
		$texte   = '<p><strong>' . _T('opquast_critere:champ_objectif_label') . '</strong></p>' . $row['objectif'];
		$texte   .= '<p><strong>' . _T('opquast_critere:champ_solution_label') . '</strong></p>' . $row['solution'];
		$texte   .= '<p><strong>' . _T('opquast_critere:champ_controle_label') . '</strong></p>' . $row['controle'];
		$texte   = str_replace('<ul>', '<ul class="spip">', $texte);
		$critere = array(
			'titre'   => $row['titre'],
			'texte'   => $texte,
			'numero'  => $key,
			'version' => 'v4',
			'statut'  => 'non_verifie',
		);

		$id_opquast_critere = objet_inserer('opquast_criteres', null, $critere);

		$tags = explode(',', $row['tag']);
		foreach ($tags as $tag) {
			if($tag) {
				if (!$id_opquast_tag = sql_getfetsel('id_opquast_tag', 'spip_opquast_tags', 'titre = ' . sql_quote($tag))) {
					$id_opquast_tag = sql_insertq('spip_opquast_tags', array('titre' => $tag));
				}
				objet_associer(array('opquast_critere' => $id_opquast_critere), array('opquast_tag' => $id_opquast_tag));
			}
		}

		$etapes = explode(',', $row['etape']);
		foreach ($etapes as $etape) {
			if($etape) {
				if (!$id_opquast_etape = sql_getfetsel('id_opquast_etape', 'spip_opquast_etapes', 'titre = ' . sql_quote($etape))) {
					$id_opquast_etape = sql_insertq('spip_opquast_etapes', array('titre' => $etape));
				}
				objet_associer(array('opquast_critere' => $id_opquast_critere), array('opquast_etape' => $id_opquast_etape));
			}
		}

		$themes = explode(',', $row['theme']);
		foreach ($themes as $theme) {
			if($theme) {
				if (!$id_opquast_theme = sql_getfetsel('id_opquast_theme', 'spip_opquast_themes', 'titre = ' . sql_quote($theme))) {
					$id_opquast_theme = sql_insertq('spip_opquast_themes', array('titre' => $theme));
				}
				objet_associer(array('opquast_critere' => $id_opquast_critere), array('opquast_theme' => $id_opquast_theme));
			}
		}
	}
}