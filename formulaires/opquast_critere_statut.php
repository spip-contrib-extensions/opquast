<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_opquast_critere_statut_charger($id_opquast_critere = null, $statut = null) {
	if(!$statut) {
		$statut = sql_getfetsel('statut', 'spip_opquast_criteres', 'id_opquast_critere = ' . intval($id_opquast_critere));
	}
	$valeurs = array(
		'id_opquast_critere' => $id_opquast_critere,
		'critere_statut'     => $statut,
	);

	return $valeurs;
}

function formulaires_opquast_critere_statut_traiter($id_opquast_critere = null, $statut = null) {
	include_spip('action/editer_objet');
	$retour = array();
	$err = objet_instituer('opquast_critere', $id_opquast_critere, array('statut' => _request('critere_statut')));
	if (!$err) {
		if (defined('_AJAX') and _AJAX) {
			if (!isset($retour['message_ok'])) {
				$retour['message_ok'] = '';
			}
			$retour['message_ok'] .= _T('info_modification_enregistree') . '
				<script>
					$(function(){
						ajaxReload("liste-objets");
						ajaxReload("navigation", {args:{recherche:"",id_opquast_tag:'.json_encode(_request('id_opquast_tag')).',statut:"'._request('statut').'"}});
					});
				</script>
				';
		}
	} else {
		$retour['message_erreur'] = $err;
	}

	return $retour;
}
