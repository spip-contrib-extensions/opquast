<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclarer les interfaces des tables pour le compilateur
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 *
 * @return array
 *     Déclarations d'interface pour le compilateur
 **/
function opquast_declarer_tables_interfaces($interfaces) {
	$interfaces['table_des_tables']['opquast_criteres']       = 'opquast_criteres';
	$interfaces['table_des_tables']['opquast_criteres_liens'] = 'opquast_criteres_liens';
	$interfaces['table_des_tables']['opquast_tags']           = 'opquast_tags';
	$interfaces['table_des_tables']['opquast_themes']         = 'opquast_themes';
	$interfaces['table_des_tables']['opquast_etapes']         = 'opquast_etapes';

	$interfaces['tables_jointures']['spip_opquast_criteres'][] = 'opquast_criteres_liens';
	$interfaces['tables_jointures']['spip_opquast_tags'][]     = 'opquast_criteres_liens';
	$interfaces['tables_jointures']['spip_opquast_themes'][]   = 'opquast_criteres_liens';
	$interfaces['tables_jointures']['spip_opquast_etapes'][]   = 'opquast_criteres_liens';

	return $interfaces;
}

/**
 * Déclarer les tables des objets
 *
 * @pipeline declarer_tables_objets_sql
 *
 * @param array $tables
 *     Description des tables
 *
 * @return array
 *     Description complétée des tables
 **/
function opquast_declarer_tables_objets_sql($tables) {

	$tables['spip_opquast_criteres'] = array(
		'type'                    => 'opquast_critere',
		'principale'              => 'oui',
		'table_objet_surnoms'     => array('opquast_critere'),
		'field'                   => array(
			'id_opquast_critere' => 'BIGINT(21) NOT NULL',
			'numero'             => 'BIGINT(21) NOT NULL',
			'version'            => 'VARCHAR(5) NOT NULL DEFAULT ""',
			'titre'              => "TEXT NOT NULL DEFAULT ''",
			'lien'               => "TEXT NOT NULL DEFAULT ''",
			'texte'              => "TEXT NOT NULL DEFAULT ''",
			'statut'             => 'VARCHAR(20) NOT NULL DEFAULT ""',
			'maj'                => 'TIMESTAMP',
		),
		'key'                     => array(
			'PRIMARY KEY' => 'id_opquast_critere',
			'KEY statut'  => 'statut',
		),
		'titre'                   => 'titre AS titre, "" AS lang',
		'champs_editables'        => array(
			'titre',
			'texte',
			'numero',
			'version',
		),
		'champs_versionnes'       => array(),
		'rechercher_champs'       => array(
			'titre' => 10,
			'texte' => 5,
		),
		'statut_textes_instituer' => array(
			'non_verifie'    => 'opquast_critere:texte_statut_non_verifie',
			'non_conforme'   => 'opquast_critere:texte_statut_non_conforme',
			'conforme'       => 'opquast_critere:texte_statut_conforme',
			'non_applicable' => 'opquast_critere:texte_statut_non_applicable',
		),
		'statut'                  => array(
			array(
				'champ'     => 'statut',
				'publie'    => '',
				'previsu'   => '',
				'post_date' => '',
				'exception' => array(
					'statut',
					'tout',
				),
			),
		),
		'statut_images'           => array(
			'non_verifie'    => 'puce-non_verifie-8.png',
			'non_conforme'   => 'puce-non_conforme-8.png',
			'conforme'       => 'puce-conforme-8.png',
			'non_applicable' => 'puce-non_applicable-8.png',
		),
		'texte_changer_statut'    => 'opquast_critere:texte_changer_statut_opquast_critere',
		'tables_jointures'        => array(),
		'page'                    => false,
	);

	return $tables;
}

/**
 * Déclarer les tables principales
 *
 * @pipeline declarer_tables_principales
 *
 * @param array $tables
 *     Description des tables
 *
 * @return array
 *     Description complétée des tables
 **/
function opquast_declarer_tables_principales($tables) {

	$tables['spip_opquast_tags'] = array(
		'field' => array(
			'id_opquast_tag' => 'BIGINT(21) NOT NULL',
			'titre'          => "TEXT NOT NULL DEFAULT ''",
			'maj'            => 'TIMESTAMP',
		),
		'key'   => array(
			'PRIMARY KEY' => 'id_opquast_tag',
		),
	);

	$tables['spip_opquast_themes'] = array(
		'field' => array(
			'id_opquast_theme' => 'BIGINT(21) NOT NULL',
			'titre'            => "TEXT NOT NULL DEFAULT ''",
			'maj'              => 'TIMESTAMP',
		),
		'key'   => array(
			'PRIMARY KEY' => 'id_opquast_theme',
		),
	);

	$tables['spip_opquast_etapes'] = array(
		'field' => array(
			'id_opquast_etape' => 'BIGINT(21) NOT NULL',
			'titre'            => "TEXT NOT NULL DEFAULT ''",
			'maj'              => 'TIMESTAMP',
		),
		'key'   => array(
			'PRIMARY KEY' => 'id_opquast_etape',
		),
	);

	return $tables;
}

/**
 * Déclarer les tables auxiliaires
 *
 * @pipeline declarer_tables_auxiliaires
 *
 * @param array $tables
 *     Description des tables
 *
 * @return array
 *     Description complétée des tables
 **/
function opquast_declarer_tables_auxiliaires($tables) {

	$tables['spip_opquast_criteres_liens'] = array(
		'field' => array(
			'id_opquast_critere' => "bigint(21) DEFAULT '0' NOT NULL",
			'id_objet'           => "bigint(21) DEFAULT '0' NOT NULL",
			'objet'              => "VARCHAR (25) DEFAULT '' NOT NULL",
		),
		'key'   => array(
			'PRIMARY KEY'            => 'id_opquast_critere,id_objet,objet',
			'KEY id_opquast_critere' => 'id_opquast_critere',
		),
	);

	return $tables;
}