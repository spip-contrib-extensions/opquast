# Check-list Opquast

Plugin SPIP pour suivre la check-list de référence Qualité Web v4 définie par Opquast.

Ce plugin peut s'installer, comme tous les autres plugins, depuis l'espace privé de SPIP, dans le menu Configuration -> Gestion des plugins, en cherchant le terme "opquast" dans le moteur de recherche.

A terme, ce plugin pourra évoluer pour permettre de gérer plusieurs check-lists, probablement avec un plugin générique / API et des plugins dédiés fournissant les données.

## Todo

[ ] Pouvoir ajouter une note (textarea) sur chaque critère
- notes liées aux auteurs ? ou globale ?
 
[x] Intégrer les fiches V4 (version 2020) dès qu'elles seront disponibles 

[ ] Générer des rapports (html ou PDF)

[x] Pouvoir gérer plusieurs check-lists (champ 'version' dans la table spip_opquast_criteres)

[ ] Déclarer un pipeline utilisable par des plugins/squelettes qui leur permettrait de déclarer conformes certains critères (par exemple : il y a une page 404)
- déclarer un identifiant unique pour chaque critère (hors id autoincrement) 
- déclarer un identifiant unique pour checklist / version de checklist 