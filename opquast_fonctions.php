<?php
/**
 * Fonctions utiles au plugin Check-list Opquast
 *
 * @plugin     Check-list Opquast
 * @copyright  2020
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\opquast\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function opquast_get_admins_ids() {
	$id_admins = preg_replace('#[^\d]+#', ',', trim(lire_config('opquast/admins')));

	return explode(',', $id_admins);
}

function lien_opquast_critere($lien) {
	return 'https://checklists.opquast.com'.$lien;
}